# Danish translation gnome-system-log.
# Copyright (C) 2012 gnome-system-log & nedenstående oversættere.
# This file is distributed under the same license as the gnome-system-log package.
# Joe Hansen <joedalton2@yahoo.dk>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-system-log\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-10-15 17:04+0200\n"
"PO-Revision-Date: 2012-10-15 17:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../debian/org.debian.pkexec.gnome-system-log.policy.in.h:1
msgid "Run gnome-system-log"
msgstr "Kør gnome-system-log"

#: ../../debian/org.debian.pkexec.gnome-system-log.policy.in.h:2
msgid "Authentication is required to view system logs"
msgstr "Godkendelse er krævet for at se systemlogge"
